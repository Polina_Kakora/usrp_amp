﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using AwsToConsoleTransferLib;
using UsrpLib;

namespace TestUsrpLib
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        UsrpManager usrpManager;
        string text = "";
        public string TextMess
        {
            get { return text; }
            set
            {
                text = value;
                PropertyChanged(this, new PropertyChangedEventArgs("TextMess"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
            
            //usrpManager = new UsrpManager(9121,"192.168.0.115", 3, 0);
            usrpManager = new UsrpManager(9121, "0.0.0.0", 2, 3);
            SubscribeToManagerEvents();
        }

        private void SubscribeToManagerEvents()
        {
            usrpManager.OnMessToMain += UsrpManager_OnMessToMain;
            usrpManager.OnConnectedFromUsrp += UsrpManager_OnConnectedFromUsrp;
            usrpManager.OnConnectFailedFromUsrp += UsrpManager_OnConnectFailedFromUsrp;
            usrpManager.OnDisconnectedFromUsrp += UsrpManager_OnDisconnectedFromUsrp;
        }

        private void UsrpManager_OnDisconnectedFromUsrp(object sender, EventArgs e)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Connection.IsEnabled = true;
            //});

            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Disconnection.IsEnabled = false;
            //});
            
        }

        private void UsrpManager_OnConnectFailedFromUsrp(object sender, EventArgs e)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Connection.IsEnabled = true;
            //});

            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Disconnection.IsEnabled = true;
            //});
        }

        private void UsrpManager_OnConnectedFromUsrp(object sender, EventArgs e)
        {
            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Connection.IsEnabled = false;
            //});

            //Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            //{
            //    Disconnection.IsEnabled = true;
            //});
        }

        private void UsrpManager_OnMessToMain(object sender, string e)
        {
            TextMess += e + "\n" + "\n";
        }

        private void Disconnection_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.Disconnect();
            
        }

        private void Connection_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.Connect(Convert.ToInt32(tbPort.Text), tbIp.Text);
            
        }

        private void BtnSetParams_Click(object sender, RoutedEventArgs e)
        {
            //usrpManager.SetControlParams(new ParametersReal(new byte[26] ));
            usrpManager.SetControlParams(new ControlParameters(Convert.ToInt16(tbElevation.Text), Convert.ToInt32(tbSpeed.Text), Convert.ToInt16(tbDirect.Text), (byte)Convert.ToInt32(tbRangeGate.Text)));
        }

        private void BtnSpoofOff_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.SpoofingOff();
        }

        private void BtnRequestState_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.GetGeneratorState();
        }

        private void BtnRequestParams_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.GetParams();
        }

        private void BtnRequestFake_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.GetFakeCoords();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            TextMess = "";
        }

        private void BtnSpoofOn_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.SpoofingOn();
        }

        private void BtnRequestFullState_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.GetDetailedGeneratorState();
        }

        private void btnSetCoords_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.SetStartCoordinates(new CoordinatesParameters(Convert.ToDouble(tbLatitude.Text), Convert.ToDouble(tbLongitude.Text)));

        }

        private void BtnSpoofOnType_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.SpoofingOn((CodesGnssTypes)cmbGnssType.SelectedIndex + 1);
            
        }

        private void BtnGetSimulateSats_Click(object sender, RoutedEventArgs e)
        {
            usrpManager.GetSimulatedSats();
        }
    }
}
