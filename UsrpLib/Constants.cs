﻿
namespace UsrpLib
{
    internal static class Constants
    {
        public const byte AddressSender = 2;
        public const byte AddressRecipient = 4;

        public const int HeaderLength = 8;
        public const int InfoLengthField = 4;

        public const int WaitTimeMs = 500;  //время работы таймера
        public const int Infinite = -1;
    }
}
