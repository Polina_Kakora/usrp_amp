﻿using System;
using AwsToConsoleTransferLib;

namespace UsrpLib
{
    public class SpoofGenerator : SendProcessing, ISpoofGenerator
    {
        public SpoofGenerator(int myPort, string myIP, byte addressSender, byte addressRecipient)
            : base(myPort, myIP, addressSender, addressRecipient)
        {
        }

        /// <summary>
        /// Request state of generator
        /// </summary>
        public bool GetGeneratorState()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.REQUEST_STATE, null);
        }


        /// <summary>
        /// Request state of generator and generated satellites
        /// </summary>
        public bool GetDetailedGeneratorState()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.REQUEST_STATE_DETAILED, null);
        }


        /// <summary>
        /// Request simulated satellites
        /// </summary>
        public bool GetSimulatedSatellites()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.REQUEST_SIMULATED_SATS, null);
        }


        /// <summary>
        /// Set control parameters 
        /// </summary>
        public bool SetControlParams(ControlParameters data)
        {
            try
            {
                return PackCodogram(Constants.HeaderLength + 9, (byte)CodesUsrp.SET_PARAM,
                    delegate (ref byte[] ForSend)
                    {
                        Array.Copy(BitConverter.GetBytes(data.Elevation), 0, ForSend, Constants.HeaderLength, 2);
                        Array.Copy(BitConverter.GetBytes(data.Speed), 0, ForSend, Constants.HeaderLength + 2, 4);
                        Array.Copy(BitConverter.GetBytes(data.HeadingDeg), 0, ForSend, Constants.HeaderLength + 6, 2);
                        ForSend[Constants.HeaderLength + 8] = data.RangeGate;
                    });
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Turn on spoofing mode
        /// </summary>
        public bool SpoofingOn()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.SPOOF_ON, null);
        }


        /// <summary>
        /// Turn on spoofing mode with type
        /// </summary>
        public bool SpoofingOn(CodesGnssTypes type)
        {
            try
            {
                return PackCodogram(Constants.HeaderLength + 1, (byte)CodesUsrp.SPOOF_ON_WITH_TYPE,
                    delegate (ref byte[] ForSend)
                    {
                        ForSend[8] = (byte)type;
                    });
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Turn on spoofing mode with type and amplifier number
        /// </summary>
        public bool SpoofingOn(CodesGnssTypes type, byte numAM)
        {
            try
            {
                return PackCodogram(Constants.HeaderLength + 2, (byte)CodesUsrp.SPOOF_ON_WITH_TYPE_AM,
                    delegate (ref byte[] ForSend)
                    {
                        ForSend[8] = (byte)type;
                        ForSend[9] = numAM;
                    });
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Turn on spoofing mode 4 systems
        /// </summary>
        public bool SpoofingOn(bool gps, bool glonass, bool beidou, bool galileo)
        {
            try
            {
                return PackCodogram(Constants.HeaderLength + 4, (byte)CodesUsrp.SPOOF_ON_FOUR_SYST,
                    delegate (ref byte[] ForSend)
                    {
                        ForSend[8] = gps ? (byte)1 : (byte)0;
                        ForSend[9] = glonass ? (byte)1 : (byte)0;
                        ForSend[10] = beidou ? (byte)1 : (byte)0;
                        ForSend[11] = galileo ? (byte)1 : (byte)0;
                    });
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// Turn off spoofing mode
        /// </summary>
        public bool SpoofingOff()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.SPOOF_OFF, null);
        }


        /// <summary>
        /// Request generator parameters 
        /// </summary>
        public bool GetParams()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.REQUEST_PARAM, null);
        }


        /// <summary>
        /// Request fake coordinates
        /// </summary>
        public bool GetFakeCoords()
        {
            return PackCodogram(Constants.HeaderLength, (byte)CodesUsrp.REQUEST_FAKE, null);
        }


        /// <summary>
        /// Set start coordinates
        /// </summary>
        public bool SetStartCoord(CoordinatesParameters data)
        {
            try
            {
                return PackCodogram(Constants.HeaderLength + 16, (byte)CodesUsrp.SET_START_COORD,
                    delegate (ref byte[] ForSend)
                    {
                        Array.Copy(BitConverter.GetBytes(data.LatitudeDeg), 0, ForSend, Constants.HeaderLength, 8);
                        Array.Copy(BitConverter.GetBytes(data.LongitudeDeg), 0, ForSend, Constants.HeaderLength + 8, 8);
                    });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
