﻿using System;
using System.Threading;

namespace UsrpLib
{
    public class SendProcessing : ReceiveProcessing
    {
        protected SendProcessing(int myPort, string myIP, byte addressSender, byte addressRecipient)
            : base(myPort, myIP, addressSender, addressRecipient)
        {
            //var lossesCount = new TimerCallback(LossesCount);
            //Timer = new Timer(lossesCount, timeOutFlag, 20000, Constants.Infinite); //для ожидания ответа
            DisconnectNet += OnDisconnectNet;
            ConnectionFailed += OnDisconnectNet;
        }


        public event EventHandler<byte> SendCmd;
        public event EventHandler LostGenerator;
        protected delegate void FillMainParams(ref byte[] ArrForSend); 
        
        
        protected bool PackCodogram(int codogramLenght, byte code, FillMainParams some_del)
        {
            var arrayForSend = new byte[codogramLenght];

            try
            {
                arrayForSend[0] = addressSender;
                arrayForSend[1] = addressRecipient;
                arrayForSend[2] = code;
                countSendCmd = Convert.ToByte((countSendCmd == 255 ? 0 : countSendCmd + 1));
                arrayForSend[3] = countSendCmd;
                Array.Copy(BitConverter.GetBytes(arrayForSend.Length - Constants.HeaderLength), 0, arrayForSend, 4, 4); // TODO: возможно переворачивать байты
                //место для делегата, который заполнит или нет (если null) информационную часть, различную для всех кодограмм
                if (some_del != null)
                    some_del.Invoke(ref arrayForSend);
                //while (!canSendData)
                //{
                //    Thread.Sleep(10);
                //}
                //Timer.Change(Constants.Infinite, Constants.Infinite);

                if (WriteData(arrayForSend))
                {
                    
                    SendCmd?.Invoke(this, code);
                    //SentPackagesQueue.Enqueue(code);
                    step++;
                    //Timer.Change(Constants.WaitTimeMs, Constants.Infinite);
                    //canSendData = false;
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        //считает +1 потерю, если таймер закончился
        private void LossesCount(object state)
        {
            lossCount += 1;
            //если не пришел 1 ответ, начать отсчет шагов (до 3)
            if (lossCount == 1)
            {
                step = 1;
            }

            timeOutFlag = true;
            try
            {
                SentPackagesQueue.Dequeue();
            }
            catch 
            { }
            canSendData = true;
            
            //проверка на 3 подряд не пришедших ответа
            if ((lossCount == 3) && (step == lossCount))
            {
                LostGenerator?.Invoke(this, EventArgs.Empty);
                lossCount = 0;
            }

            //обнуление счетчиков, если не 3 шага подряд не пришел ответ
            if (step == 3 || lossCount == 3)
            {
                step = 0;
                lossCount = 0;
            }
        }


        private void OnDisconnectNet(object sender, EventArgs e)
        {
            //if (Timer != null)
            //{
            //    Timer.Dispose();
            //}
        }
    }
}
