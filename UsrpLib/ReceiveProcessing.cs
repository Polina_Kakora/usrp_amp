﻿using System;
using System.Collections.Generic;
using AwsToConsoleTransferLib;

namespace UsrpLib
{
    public class ReceiveProcessing : TCP
    {
        #region Codogramm counters
        protected byte countSendCmd;
        protected byte countReceiveCmd;
        #endregion

        #region Addresses
        protected readonly byte addressSender;
        protected readonly byte addressRecipient;
        #endregion

        #region Loss counting
        //protected static Timer Timer;
        protected static Queue<byte> SentPackagesQueue;
        protected static bool timeOutFlag = false;
        protected static bool canSendData = true;  
        protected static int lossCount = 0;  
        protected byte step = 0;
        #endregion


        public event EventHandler<BaseReplyEventArgs> ReplyCommand;
        public event EventHandler<StateReplyEventArgs> ReplySpoofMode;
        public event EventHandler<DetailedStateReplyEventArgs> ReplySpoofModeDetailed;
        public event EventHandler<ParamsRealEventArgs> ReplyTrueCoords;
        public event EventHandler<ParamsFakeEventArgs> ReplyFakeCoords;
        public event EventHandler<SimulatedSatsEventArgs> ReplySimulatedSats;
        protected event EventHandler SuccessSetParams;


        protected ReceiveProcessing(int myPort, string myIP, byte addressSender, byte addressRecipient)
            : base(myPort, myIP)
        {
            this.addressSender = addressSender;
            this.addressRecipient = addressRecipient;
            countSendCmd = countReceiveCmd =  0;
            ReceiveByte += ReceiveProcessing_ReceiveByte;
            SentPackagesQueue = new Queue<byte>();
        }
        

        private void ReceiveProcessing_ReceiveByte(object sender, byte[] data)
        {
            byte code = data[2];
            if (Enum.IsDefined(typeof(CodesUsrp), code))
            {
                //CheckCode(code);
                DecodeInfoPart(code, data);
            }
        }


        private void CheckCode(byte code)
        {
            if (SentPackagesQueue.Count == 0 || code != SentPackagesQueue.Peek())
                return;

            //Timer.Change(Constants.Infinite, Constants.Infinite);
            SentPackagesQueue.Dequeue();
            canSendData = true;
        }


        private void DecodeInfoPart(byte code, byte[] data)
        {
            var codeError = data[Constants.HeaderLength]; //TODO: добавить анализ ошибки
            var inform = new byte[data.Length - (Constants.HeaderLength + 1)];
 
            Array.Copy(data, Constants.HeaderLength + 1, inform, 0, inform.Length);

            RaiseCurrentEvent(code, codeError, inform);
        }


        private void RaiseCurrentEvent(byte code, byte error, byte[] infoPart)
        {
            if (infoPart.Length == 0)
            {
                ReplyCommand?.Invoke(this, new BaseReplyEventArgs(code, error));

                if (code == (byte)CodesUsrp.SET_PARAM && error == 0)
                {
                    SuccessSetParams?.Invoke(this, EventArgs.Empty);
                }
                return;
            }

            switch (code)
            {
                case (byte)CodesUsrp.REQUEST_STATE:
                    ReplySpoofMode?.Invoke(this, new StateReplyEventArgs(code, error, infoPart));
                    break;
                case (byte)CodesUsrp.REQUEST_STATE_DETAILED:
                    ReplySpoofModeDetailed?.Invoke(this, new DetailedStateReplyEventArgs(code, error, infoPart));
                    break;
                case (byte)CodesUsrp.REQUEST_PARAM:
                    ReplyTrueCoords?.Invoke(this, new ParamsRealEventArgs(code, error, infoPart));
                    break;
                case (byte)CodesUsrp.REQUEST_FAKE:
                    ReplyFakeCoords?.Invoke(this, new ParamsFakeEventArgs(code, error, infoPart));
                    break;
                case (byte)CodesUsrp.REQUEST_SIMULATED_SATS:
                    ReplySimulatedSats?.Invoke(this, new SimulatedSatsEventArgs(code, error, infoPart));
                    break;
                default:
                    break;
            }
        }
        
    }
}
