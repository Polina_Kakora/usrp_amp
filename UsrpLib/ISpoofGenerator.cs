﻿using AwsToConsoleTransferLib;
using System;

namespace UsrpLib
{
    interface ISpoofGenerator
    {
        bool Connect(int generatorPort, string generatorIp);
        void Disconnect();
        
        bool GetGeneratorState();
       // bool SetControlParams(ParametersReal data);
        bool SpoofingOn(); 
        bool SpoofingOff();
        bool GetParams();
        bool GetFakeCoords();


        event EventHandler ConnectNet; 
        event EventHandler ConnectionFailed;
        event EventHandler DisconnectNet; 
        event EventHandler<byte[]> ReceiveByte; 
        event EventHandler<byte[]> SendByte;
        event EventHandler<byte> SendCmd;

        event EventHandler<BaseReplyEventArgs> ReplyCommand;
        event EventHandler<StateReplyEventArgs> ReplySpoofMode;
        event EventHandler<ParamsRealEventArgs> ReplyTrueCoords;
        
    }
}
