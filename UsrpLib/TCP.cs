﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace UsrpLib
{
    public class TCP
    {
        private readonly int _myPort;
        private readonly string _myIp;

        private TcpClient _tcpClient;
        private NetworkStream _streamClient;
        private IPEndPoint _localIpEndPoint;
        private Thread _thrRead;


        protected TCP(int myPort, string myIP)
        {
            _myPort = myPort;
            _myIp = myIP;
        }

        public event EventHandler ConnectNet;
        public event EventHandler ConnectionFailed;
        public event EventHandler DisconnectNet;
        public event EventHandler<byte[]> SendByte;
        public event EventHandler<byte[]> ReceiveByte;

        protected virtual void OnWithoutParam(EventHandler some_ev) => some_ev?.Invoke(this, null);
        protected virtual void OnByteArray(EventHandler<byte[]> some_ev, byte[] data) => some_ev?.Invoke(this, data);


        #region Commands
        /// <summary>
        /// Connect to USRP server 
        /// </summary>
        public bool Connect(int generatorPort, string generatorIp)
        {
            if (_tcpClient != null)
            {
                _tcpClient.Close();
            }

            //_localIpEndPoint = new IPEndPoint(IPAddress.Parse(_myIp), _myPort);
            //_tcpClient = new TcpClient(_localIpEndPoint); 
            _tcpClient = new TcpClient();

            try
            {
                var result = _tcpClient.BeginConnect(generatorIp, generatorPort, null, null);
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));

                if (!success)
                {
                    OnWithoutParam(ConnectionFailed);
                }
                
                _tcpClient.EndConnect(result);
            }
            catch (Exception e)
            {
                OnWithoutParam(ConnectionFailed);
                return false;
            }

            
            if (_tcpClient.Connected)
            {
                if (_streamClient != null)
                {
                    _streamClient = null;
                }
                _streamClient = _tcpClient.GetStream();


                if (_thrRead != null)
                {
                    _thrRead.Abort();
                    _thrRead.Join(500);
                    _thrRead = null;
                }

                _thrRead = new Thread(ReadData);
                 _thrRead.IsBackground = true;
                 _thrRead.Start();
            }
                    
            OnWithoutParam(ConnectNet);
            return true;
        }


        /// <summary>
        /// Disconnect from USRP server 
        /// </summary>
        public void Disconnect()
        {

            if (_thrRead != null)
            {
                try
                {
                    _thrRead.Abort();
                    _thrRead.Join(500);
                    _thrRead = null;
                }
                catch (Exception ex)
                {}
            }


            if (_streamClient != null)
            {
                try
                {
                    _streamClient.Close();
                    _streamClient = null;
                }
                catch (Exception ex)
                { }
            }
            
            if (_tcpClient != null)
            {
                try
                {
                    _tcpClient.Close();
                    _tcpClient = null;
                }
                catch (Exception ex)
                { }
                
            }

            OnWithoutParam(DisconnectNet);
        }
        #endregion


        private void ReadData()
        {
            int readLength = 0;
            int currSize = 0;
            while (true)
            {
                try
                {
                    byte[] dataRead = null;
                    byte[] buffer = null;
                    currSize = 0;

                    Array.Resize(ref buffer, Constants.HeaderLength);
                    readLength = _streamClient.Read(buffer, 0, Constants.HeaderLength); //тут остановился до прихода сообщений

                    //считаем кол-во байт в инф. части
                    byte[] infoLengthArray = new byte[Constants.InfoLengthField];
                    Array.Copy(buffer, Constants.InfoLengthField, infoLengthArray, 0, Constants.InfoLengthField);
                    Array.Reverse(infoLengthArray);
                    int infoLength = BitConverter.ToInt32(infoLengthArray, 0);

                    Array.Resize(ref dataRead, infoLength + Constants.HeaderLength);
                    Array.Resize(ref buffer, infoLength + Constants.HeaderLength);
                    Array.Copy(buffer, 0, dataRead, 0, Constants.HeaderLength);
                    currSize += readLength;

                    while (currSize < dataRead.Length)
                    {
                        readLength = _streamClient.Read(buffer, 0, dataRead.Length - currSize);

                        Array.Copy(buffer, 0, dataRead, currSize, readLength);
                        currSize += readLength;

                        if (readLength == 0)
                        {
                            Disconnect();
                        }
                    }

                    if (dataRead.Length != 0)
                    {
                        OnByteArray(ReceiveByte, dataRead);
                    }
                }
                catch (Exception ex)
                {
                    Disconnect();
                    //OnWithoutParam(ConnectionFailed);
                    return;
                }
            }
        }


        protected bool WriteData(byte[] bSend)
        {
            try
            {
                _streamClient?.Write(bSend, 0, bSend.Length);
                OnByteArray(SendByte, bSend);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
