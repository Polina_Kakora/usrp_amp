﻿

namespace UsrpLib
{
    public struct ParamsEventArgs
    {
        public byte Code;
        public byte Error;
        public byte[] Inform;

        public ParamsEventArgs(byte code, byte error, byte[] inform)
        {
            Code = code;
            Error = error;
            Inform = inform;
        }
    }
}
