﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO.Ports;
using System.Reflection;
using System.Diagnostics;

namespace ConsoleAJS
{
    class MainProg
    {
        //private readonly SpoofGenerator SpoofGenerator;
        private static UsrpManager _usrpManager;
        private static AmpManager _ampManager;
        private static ClientGnssManager _clientGnssManager;

        static void Main(string[] args)
        {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;

            bool existed;
            // получаем GIUD приложения
            string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();
            Mutex mutexObj = new Mutex(true, guid, out existed);
            if (!existed)
            {
                Console.WriteLine("The application is already running!");
                Thread.Sleep(3000);
                return;
            }
            
            Console.Title = "USRP & AMP manager";
            Console.CursorVisible = false;
            Console.WriteLine("USRP & AMP manager was started" + "\n");

            _usrpManager.OnMessToMain += (sender, e) => { Console.ForegroundColor = ConsoleColor.DarkGreen; Console.WriteLine(e); };
            _ampManager.OnMessToMain += (sender, e) => { Console.ForegroundColor = ConsoleColor.DarkYellow; Console.WriteLine(e); };
            _clientGnssManager.OnMessToMain += (sender, e) => { Console.ForegroundColor = ConsoleColor.DarkMagenta; Console.WriteLine(e); };

            _usrpManager = new UsrpManager(9120, "127.0.0.1", 2, 4); //TO DO: читать из конфига
            _ampManager = new AmpManager(2, 4); //TO DO: читать из конфига
            _clientGnssManager = new ClientGnssManager();

            ManagersConnection();
            //new Control();



            while (true)
            {
                Console.ReadKey();
            }
            Environment.Exit(0);
            
        }

        private static void ManagersConnection()
        {
            try
            {
                _usrpManager.Connect(9120, "127.0.0.1"); 
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e);
                Console.ReadLine();
                return;
            }

            try
            {
                _ampManager.OpenPort("COM3", 9600, Parity.None, 8, StopBits.One); //TODO: читать из конфига
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e);
                Console.ReadLine();
                return;
            }
        }
    }
}
