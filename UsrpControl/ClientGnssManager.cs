﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ClientLib;
using ClientServerLib;
using ToRinexConverter;

namespace ConsoleAJS
{
    public class ClientGnssManager : IManager
    {
        public bool IsConnected { get; private set; }

        private Client _clientGnss;
        private readonly AlmanacConverter _almanacConverter;
        private readonly EphemerisConverter _ephemerisConverter;

        public event EventHandler<string> OnMessToMain;

        public ClientGnssManager()
        {
            _almanacConverter = new AlmanacConverter();
            _ephemerisConverter = new EphemerisConverter();
        }


        #region Commands

        public void Connect(string host, int port)
        {
            if (IsConnected)
                return;
          
            _clientGnss = new Client(host, port);
            if (_clientGnss.CheckHandler())
            {
                SubscribeToLibEvents();
            }
            _clientGnss.Connect();
        }
        

        public void Disconnect()
        {
            if (!IsConnected)
                return;
            
            _clientGnss.Disconnect();
            UnsubscribeFromLibEvents();
            _clientGnss = null;
        }


        public void SendRequest(byte command)
        {
            if (!IsConnected)
                return;
            _clientGnss.SendMessage((Operations)command);
        }
        #endregion


        public void SubscribeToLibEvents()
        {
            _clientGnss.OnGetData += ProcessIncomeData;
            _clientGnss.OnConnected += OnConnected;
            _clientGnss.OnDisconnected += OnDisconnected;
            _clientGnss.OnError += OnError;
        }
        

        public void UnsubscribeFromLibEvents()
        {
            _clientGnss.OnGetData -= ProcessIncomeData;
            _clientGnss.OnConnected -= OnConnected;
            _clientGnss.OnDisconnected -= OnDisconnected;
            _clientGnss.OnError -= OnError;
        }


        #region EventHandlers
        public void ProcessIncomeData(object sender, MyEventArgsGetData e)
        {
            try
            { 
                //TODO: пересмотреть обработку. Не надо выводить так подробно?
                switch (e.Command) 
                {
                    case 1:
                        //преобразование в файлы для спуфинга
                        if (_clientGnss.ephemeries.Count == 32)
                        {
                            _ephemerisConverter.WriteToRinex(null, _clientGnss.ephemeries, 1);
                            OnMessToMain(sender, "Ephemeris RINEX is ready");
                        }
                        break;
                    case 2:
                        if (_clientGnss.almanac.Count == 32)
                        {
                            _almanacConverter.WriteToSEM(null, _clientGnss.almanac, 1);
                            OnMessToMain(sender, "Almanac SEM is ready");
                        }
                        break;
                    case 4:
                        OnMessToMain(sender, "Get satellites info"); //нужно ли обрабатывать?
                        break;
                    case 5:
                        string fullInfo = "";
                        IEnumerable<FieldInfo> fieldsLoc = _clientGnss.reclocation.GetType().GetTypeInfo().DeclaredFields;

                        foreach (var field in fieldsLoc.Where(x => !x.IsStatic))
                        {
                            fullInfo += field.Name + " = " + field.GetValue(_clientGnss.reclocation) + "\n";
                        }

                        OnMessToMain(sender, "Receiver location: " + fullInfo); //нужно ли обрабатывать?
                        break;
                    case 6:
                        OnMessToMain(sender, "Time: " + _clientGnss.time.ToString("dd-MM-yyyy H:mm:s"));
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                OnMessToMain(sender, ex.Message);
            }
        }


        private void OnDisconnected(object sender, bool e)
        {
            IsConnected = true;
            OnMessToMain(sender, "Connected to GNSS server");
        }


        private void OnConnected(object sender, bool e)
        {
            IsConnected = true;
            OnMessToMain(sender, "Disconnected from GNSS server");
        }


        private void OnError(object sender, string e)
        {
            OnMessToMain(sender, e);
        }
        #endregion
    }
}
