﻿using System;
using UsrpLib;
using AwsToConsoleTransferLib;

namespace ConsoleAJS
{
    public class UsrpManager : IManager
    {
        private readonly SpoofGenerator _spoofGen;
        public bool IsConnected { get; private set; }

        public event EventHandler<string> OnMessToMain;
        //public event EventHandler<byte> OnSpoofMode;
        //public event EventHandler<ParametersFake> OnTrueParams;
        //public event EventHandler<ParametersFake> OnFakeParams;
        //public static event EventHandler<ReplyUsrpCodogramm> OnUsrpCodogramm;

        public UsrpManager(int myPort, string myIP, byte addressSender, byte addressRecipient)
        {
            _spoofGen = new SpoofGenerator(myPort, myIP, addressSender, addressRecipient);
            SubscribeToLibEvents();
        }


        #region Commands
        public bool Connect(int generatorPort, string generatorIp)
        {
            if (IsConnected)
                return true;
            return _spoofGen.Connect(generatorPort, generatorIp);
        }


        public void Disconnect()
        {
            if (!IsConnected)
                return;
            
            _spoofGen.Disconnect();

        }


        public void GetGeneratorState()
        {
            if (!IsConnected)
                return;

            _spoofGen.GetGeneratorState();
        }

        public void SetParams(ControlParameters data) //после отправки, при error = 0 в ответе, автоматом отправит SpoofingOn
        {
            if (!IsConnected)
                return;

            _spoofGen.SetControlParams(data);
        }

        public void SpoofingOff()
        {
            if (!IsConnected)
                return;

            _spoofGen.SpoofingOff();
        }

        public void GetParams()
        {
            if (!IsConnected)
                return;

            _spoofGen.GetParams();
        }

        public void GetFakeCoords()
        {
            if (!IsConnected)
                return;
            
            _spoofGen.GetFakeCoords();
        }
        #endregion


        public void SubscribeToLibEvents()
        {
            _spoofGen.ConnectNet += OnConnected;
            _spoofGen.DisconnectNet += OnDisconnected;
            _spoofGen.ConnectionFailed += OnConnectionFailed;
            _spoofGen.LostGenerator += OnLostGenerator;
            _spoofGen.SendCmd += OnSendCmd;
            _spoofGen.ReplyCommand += OnReplyCommand;
            _spoofGen.ReplyTrueCoords += OnReplyTrueCoords;
            _spoofGen.ReplyFakeCoords += OnReplyFakeCoords;
            _spoofGen.ReplySpoofMode += OnReplySpoofMode;
        }

       

        public void UnsubscribeFromLibEvents()
        {
            _spoofGen.ConnectNet -= OnConnected;
            _spoofGen.DisconnectNet -= OnDisconnected;
            _spoofGen.ConnectionFailed -= OnConnectionFailed;
            _spoofGen.LostGenerator -= OnLostGenerator;
            _spoofGen.SendCmd -= OnSendCmd;
            _spoofGen.ReplyCommand -= OnReplyCommand;
            _spoofGen.ReplyTrueCoords -= OnReplyTrueCoords;
            _spoofGen.ReplyFakeCoords -= OnReplyFakeCoords;
            _spoofGen.ReplySpoofMode -= OnReplySpoofMode;
        }


        #region EventHandlers

        private void OnConnected(object sender, EventArgs e)
        {
            IsConnected = true;
            OnMessToMain(sender, "Connected to USRP server");
        }


        private void OnDisconnected(object sender, EventArgs e)
        {
            IsConnected = false;
            OnMessToMain(sender, "Disconnected from USRP server");
            UnsubscribeFromLibEvents();

        }


        private void OnConnectionFailed(object sender, EventArgs e)
        {
            IsConnected = false;
            OnMessToMain(sender, "Can't connect to USRP server");
            UnsubscribeFromLibEvents();

        }


        private void OnLostGenerator(object sender, EventArgs e)
        {
            OnMessToMain(sender, "USRP server is lost");
        }


        private void OnSendCmd(object sender, byte code)
        {
            OnMessToMain(sender, "Send command " + ((CodesUsrp)code).ToString());
        }


        private void OnReplySpoofMode(object sender, StateReplyEventArgs e)
        {
            OnMessToMain(sender, "Current mode: " + ((Modes)e.State).ToString());
            //OnSpoofMode(sender, args);  //КИДАТЬ ИВЕНТ ДЛЯ СЕРВЕРА, А ТОТ КОНТРОЛУ СПУФИНГА ПОШЛЕТ ДАННЫЕ
        }


        private void OnReplyTrueCoords(object sender, ParamsRealEventArgs e)
        {
            OnMessToMain(sender, $"Get request on" + (CodesUsrp)e.Code + ", " +
                    $"LatitudeDeg : {e.Coords.LatitudeDeg}," +
                    //$"SignLat : {e.Coords.SignLat}," +
                    $"LongitudeDeg : {e.Coords.LongitudeDeg}," +
                    //$"SignLong : {e.Coords.SignLong}," +
                    $"Elevation : {e.Coords.Elevation}," +
                    $"Speed :{e.Coords.Speed}," +
                    $"HeadingDeg :{e.Coords.HeadingDeg},"+
                    $"RangeGate :{e.Coords.RangeGate},"
                    );
            //OnTrueParams(sender, args);  //КИДАТЬ ИВЕНТ ДЛЯ СЕРВЕРА, А ТОТ КОНТРОЛУ СПУФИНГА ПОШЛЕТ ДАННЫЕ
        }

        private void OnReplyFakeCoords(object sender, ParamsFakeEventArgs e)
        {
            OnMessToMain(sender, $"Get request on" + (CodesUsrp)e.Code + ", " +
                    $"LatitudeDeg : {e.Coords.LatitudeDeg}," +
                    //$"SignLat : {e.Coords.SignLat}," +
                    $"LongitudeDeg : {e.Coords.LongitudeDeg}," +
                    //$"SignLong : {e.Coords.SignLong}," +
                    $"Elevation : {e.Coords.Elevation}," +
                    $"Speed :{e.Coords.Speed}," +
                    $"HeadingDeg :{e.Coords.HeadingDeg}"
                    );
        }


        private void OnReplyCommand(object sender, BaseReplyEventArgs e)
        {
            OnMessToMain(sender, "Get reply on " + e.Code + ", Error = " + e.Error); //TODO: Убрать Error, когда появится анализ ошибок

            if (e.Code == (byte)CodesUsrp.SPOOF_ON && e.Error == 0)
            {
                OnMessToMain(sender, "Starting spoofing... ");
            }
        }
        #endregion
    }
}
