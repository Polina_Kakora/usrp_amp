﻿using SHS_DLL;
using System;
using System.IO.Ports;

namespace ConsoleAJS
{
    public class AmpManager : IManager
    {
        private readonly ComSHS _amplifires;
        public bool IsConnected { get; private set; }

        public event EventHandler<string> OnMessToMain;
        //TODO: добавить ивенты для передачи клиенту

        public AmpManager(byte addressAWP, byte addressSHS)
        {
            _amplifires = new ComSHS(addressAWP, addressSHS);
            SubscribeToLibEvents();
        }


        #region Commands

        public bool OpenPort(string portName, int rate, Parity parity, int dataBits, StopBits stopBits)
        {
            if (IsConnected)
                return true;
            return _amplifires.OpenPort(portName, rate, parity, dataBits, stopBits);
        }


        public void ClosePort()
        {
            if (!IsConnected)
                return;

            _amplifires.ClosePort();
            UnsubscribeFromLibEvents();
        }


        public void SendStatus(byte letter)
        {
            if (!IsConnected)
                return;
            _amplifires.SendStatus(letter);
        }


        public void SetSpoof() 
        {
            if (!IsConnected)
                return;
            _amplifires.SendSetSPOOF();
        }


        public void SetParamFWS(int duration, TParamFWS[] paramFws)
        {
            if (!IsConnected)
                return;
            _amplifires.SendSetParamFWS(duration, paramFws);
        }


        public void SetGNSS(TGnss gnss)
        {
            if (!IsConnected)
                return;
            _amplifires.SendSetGNSS(gnss);
        }


        public void Reset(byte letter)
        {
            if (!IsConnected)
                return;
            _amplifires.SendReset(letter);
        }


        public void RadiatOff(byte letter)
        {
            if (!IsConnected)
                return;
            _amplifires.SendRadiatOff(letter);
        }
        #endregion


        public void SubscribeToLibEvents()
        {
            _amplifires.OnConfirmStatus += OnConfirmStatus;
            _amplifires.OnConfirmSet += OnConfirmSet;
            _amplifires.OnConfirmFullStatus += OnConfirmFullStatus;
        }
        

        public void UnsubscribeFromLibEvents()
        {
            _amplifires.OnConfirmStatus -= OnConfirmStatus;
            _amplifires.OnConfirmSet -= OnConfirmSet;
            _amplifires.OnConfirmFullStatus -= OnConfirmFullStatus;
        }


        #region EventHandlers

        private void OnConfirmFullStatus(object sender, ConfirmFullStatusEventArgs e)
        {
            for (int i = 0; i < e.Amplifiers.Length; i++)
            {
                OnMessToMain(sender, "Amp#" + i + 1 + " state: " + e.Amplifiers[i].ToString() + "; ");
            }

            OnMessToMain(sender, "Relay: " + e.Relay + ", Error code: " + e.CodeError.ToString());

            for (int i = 0; i < e.ParamAmp.Length; i++)
            {
                OnMessToMain(sender, $"Get status, " +
                $"Snt : {e.ParamAmp[i].Snt}," +
                $"Rad : {e.ParamAmp[i].Rad}," +
                $"Temp : {e.ParamAmp[i].Temp}," +
                $"Current : {e.ParamAmp[i].Current}," +
                $"Power : {e.ParamAmp[i].Power}," +
                $"Error :{e.ParamAmp[i].Error}"
                );
            }
        }


        private void OnConfirmSet(object sender, ConfirmSetEventArgs e)
        {
            OnMessToMain(sender, "Get reply on command " + e.AmpCode + ", Error code: " + e.CodeError.ToString());
        }


        private void OnConfirmStatus(object sender, ConfirmStatusEventArgs e)
        {
            for (int i = 0; i < e.ParamAmp.Length; i++)
            {
                OnMessToMain(sender, $"Get status, " +
                $"Snt : {e.ParamAmp[i].Snt}," +
                $"Rad : {e.ParamAmp[i].Rad}," +
                $"Temp : {e.ParamAmp[i].Temp}," +
                $"Current : {e.ParamAmp[i].Current}," +
                $"Power : {e.ParamAmp[i].Power}," +
                $"Error :{e.ParamAmp[i].Error}"
                );
            }
            //OnTrueParams(sender, args);  //TODO: КИДАТЬ ИВЕНТ ДЛЯ СЕРВЕРА, А ТОТ КОНТРОЛУ СПУФИНГА ПОШЛЕТ ДАННЫЕ
        }
        #endregion
    }
}
