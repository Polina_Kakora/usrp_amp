﻿using System;

namespace ConsoleAJS
{
    public interface IManager
    {
        bool IsConnected { get; }

        event EventHandler<string> OnMessToMain;

        void SubscribeToLibEvents();
        void UnsubscribeFromLibEvents();
    }
}
