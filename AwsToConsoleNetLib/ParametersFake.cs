﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public struct ParametersFake
    {
        public double LatitudeDeg { get; private set; }
        public double LongitudeDeg { get; private set; }
        public short Elevation { get; private set; }
        public int Speed { get; private set; }
        public short HeadingDeg { get; private set; }

        private const int coordsClassLength = 24;


        public ParametersFake(byte[] data)
        {
            if (data.Length == coordsClassLength)
            {
                Array.Reverse(data, 0, 8);
                LatitudeDeg = BitConverter.ToDouble(data, 0);

                //SignLat = LatitudeDeg >= 0 ? (byte)0 : (byte)1;

                Array.Reverse(data, 8, 8);
                LongitudeDeg = BitConverter.ToDouble(data, 8);

                //SignLong = LongitudeDeg >= 0 ? (byte)0 : (byte)1;

                Array.Reverse(data, 16, 2);
                Elevation = BitConverter.ToInt16(data, 16);
                Array.Reverse(data, 18, 4);
                Speed = BitConverter.ToInt32(data, 18);
                Array.Reverse(data, 22, 2);
                HeadingDeg = BitConverter.ToInt16(data, 22);
            }
            else
            {
                LatitudeDeg = 0;
                LongitudeDeg = 0;
                Elevation = 0;
                Speed = 0;
                HeadingDeg = 0;
            }
        }


        public ParametersFake(double latitude, double longitude, short elevation, int speed, short heading)
        {
            LatitudeDeg = latitude;
            LongitudeDeg = longitude;
            Elevation = elevation;
            Speed = speed;
            HeadingDeg = heading;
        }
    }
}
