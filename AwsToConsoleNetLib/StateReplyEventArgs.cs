﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public class StateReplyEventArgs : BaseReplyEventArgs
    {
        public byte State { get; private set; } = 255;

        public StateReplyEventArgs(byte code, byte error, byte[] infoPart) : base(code, error)
        {
            State = infoPart[0];
        }

        public StateReplyEventArgs(byte code, byte error, byte state) : base(code, error)
        {
            State = state;
        }
    }
}
