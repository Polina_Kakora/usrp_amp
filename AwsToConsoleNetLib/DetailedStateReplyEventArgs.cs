﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public class DetailedStateReplyEventArgs : StateReplyEventArgs
    {
        public List<byte> Satellites { get; private set; } = new List<byte>();

        public DetailedStateReplyEventArgs(byte code, byte error, byte[] infoPart) : base(code, error, infoPart)
        {
            if (infoPart.Length > 1)
            {
                Satellites = new List<byte>(infoPart.ToList().GetRange(1, infoPart.Length - 1));
            }
        }

        public DetailedStateReplyEventArgs(byte code, byte error, byte state, List<byte> satellites) : base(code, error, state)
        {
            Satellites = new List<byte>(satellites);
        }
    }
}
