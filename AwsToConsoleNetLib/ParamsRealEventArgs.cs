﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public class ParamsRealEventArgs : BaseReplyEventArgs
    {
        public ParametersReal Coords { get; private set; }

        public ParamsRealEventArgs(byte code, byte error, byte[] information) : base(code, error)
        {
            Coords = new ParametersReal(information);
        }
    }
}
