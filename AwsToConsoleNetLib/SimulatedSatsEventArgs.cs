﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public class SimulatedSatsEventArgs : BaseReplyEventArgs
    {
        public CodesGnssTypes Type { get; private set; }
        public List<byte> Satellites { get; private set; } = new List<byte>();

        public SimulatedSatsEventArgs(byte code, byte error, byte[] infoPart) : base(code, error)
        {
            Type = (CodesGnssTypes)infoPart[0];
            if (infoPart.Length > 1)
            {
                Satellites = new List<byte>(infoPart.ToList().GetRange(1, infoPart.Length - 1));
            }
        }

        public SimulatedSatsEventArgs(byte code, byte error, CodesGnssTypes type, List<byte> satellites) : base(code, error)
        {
            Type = type;
            Satellites = new List<byte>(satellites);
        }
    }
}
