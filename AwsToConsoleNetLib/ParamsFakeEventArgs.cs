﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public class ParamsFakeEventArgs : BaseReplyEventArgs
    {
        public ParametersFake Coords { get; private set; }

        public ParamsFakeEventArgs(byte code, byte error, byte[] information) : base(code, error)
        {
            Coords = new ParametersFake(information);
        }
    }
}
