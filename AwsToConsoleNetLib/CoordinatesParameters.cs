﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public struct CoordinatesParameters
    {
        public double LatitudeDeg { get; private set; }
        public double LongitudeDeg { get; private set; }

        private const int coordsClassLength = 16;


        public CoordinatesParameters(byte[] data)
        {
            if (data.Length == coordsClassLength)
            {
                Array.Reverse(data, 0, 8);
                LatitudeDeg = BitConverter.ToDouble(data, 0);

                Array.Reverse(data, 8, 8);
                LongitudeDeg = BitConverter.ToDouble(data, 8);
            }
            else
            {
                LatitudeDeg = 0;
                LongitudeDeg = 0;
            }
        }


        public CoordinatesParameters(double latitude, double longitude)
        {
            LatitudeDeg = latitude;
            LongitudeDeg = longitude;
        }
    }
}
