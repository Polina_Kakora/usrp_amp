﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public class BaseReplyEventArgs : EventArgs
    {
        public byte Code { get; private set; } = 0;
        public short Error { get; private set; } = -1;

        public BaseReplyEventArgs(byte code, byte error)
        {
            Code = code;
            Error = error;
        }
    }
}
