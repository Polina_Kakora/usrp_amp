﻿using System;

namespace AwsToConsoleTransferLib
{
    [Serializable]
    public struct ControlParameters
    {
        public short Elevation { get; private set; }
        public int Speed { get; private set; }
        public short HeadingDeg { get; private set; }
        public byte RangeGate { get; private set; }

        private const int coordsClassLength = 9;


        public ControlParameters(byte[] data)
        {
            if (data.Length == coordsClassLength)
            {
                Array.Reverse(data, 0, 2);
                Elevation = BitConverter.ToInt16(data, 0);
                Array.Reverse(data, 2, 4);
                Speed = BitConverter.ToInt32(data, 2);
                Array.Reverse(data, 6, 2);
                HeadingDeg = BitConverter.ToInt16(data, 6);
                RangeGate = data[8];
            }
            else
            {
                Elevation = 0;
                Speed = 0;
                HeadingDeg = 0;
                RangeGate = 0;
            }
        }


        public ControlParameters(short elevation, int speed, short heading, byte rangeGate)
        {
            Elevation = elevation;
            Speed = speed;
            HeadingDeg = heading;
            RangeGate = rangeGate;
        }
    }
}
