﻿
namespace AwsToConsoleTransferLib
{
    /// <summary>
    ///  Enumeration of commands to USRP
    /// </summary>
    public enum CodesUsrp : byte
    {
        REQUEST_STATE = 11,
        SET_PARAM,
        SPOOF_ON,
        SPOOF_OFF,
        REQUEST_PARAM,
        REQUEST_FAKE = 16,
        REQUEST_STATE_DETAILED = 18,
        SET_START_COORD,
        REQUEST_SIMULATED_SATS = 22,
        SPOOF_ON_WITH_TYPE,
        SPOOF_ON_WITH_TYPE_AM,
        SPOOF_ON_FOUR_SYST
    }

    /// <summary>
    ///  Enumeration of commands to AMP
    /// </summary>
    public enum CodesAmp : byte
    {
        RADIAT_Off = 13,
        SEND_STATUS,
        SET_PARAM_FWS,
        RESET,
        GET_GNSS,
        SET_SPOOF = 19,
        PARAM_PRESELECTOR = 23,
        PARAM_FWS_GNSS,
        RADIAT_OFF_PRESELECTOR_ON,
        FULL_STATUS
    }

    /// <summary>
    ///  Enumeration of USRP modes
    /// </summary>
    public enum Modes : byte
    {
        STOP = 0,
        SYNCHRONIZATION = 1,
        IMITATION = 2

    }

    /// <summary>
    ///  Enumeration of gnss types
    /// </summary>
    public enum CodesGnssTypes : byte
    {
        GPS = 1,
        GLONASS,
        GPS_GLONASS,
        GPS_GLONASS_2
    }



    ///// <summary>
    /////  Enumeration of gnss types
    ///// </summary>
    //public enum SatsTypes : byte
    //{
    //    GPS = 1,
    //    GLONASS
    //}
}
